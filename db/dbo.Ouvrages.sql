﻿CREATE TABLE [dbo].[Ouvrages]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Titre] VARCHAR(100) NULL, 
    [ISBN] NCHAR(10) NULL, 
    [Prox] MONEY NULL
)
