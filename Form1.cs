﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace TP3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void modeConnect_Click(object sender, EventArgs e)
        {
            //set the connection string
            string connString = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=Biblio;Integrated Security=True;Pooling=False";

            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    //retrieve the SQL Server instance version
                    string query = @"SELECT Titre FROM dbo.Ouvrages WHERE Prix BETWEEN " + numericUpDown1.Value + " AND " + numericUpDown2.Value + ";";

                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(query, conn);


                    //Set the SqlDataAdapter object
                    SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);

                    //define dataset
                    DataSet ds = new DataSet();

                    //fill dataset with query results
                    dAdapter.Fill(ds);

                    //set the DataGridView control's data source/data table
                    listBox1.DataSource = ds.Tables[0];
                    listBox1.DisplayMember = "Titre";

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                //display error message
                MessageBox.Show("Exception: " + ex.Message);
            }

        }

        private void modeDeconnect_Click(object sender, EventArgs e)
        {
            //set the connection string
            string connString = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=Biblio;Integrated Security=True;Pooling=False";

            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    //retrieve the SQL Server instance version
                    string query = @"SELECT ID, Titre, ISBN, Prix FROM dbo.Ouvrages WHERE Prix BETWEEN " + numericUpDown1.Value + " AND " + numericUpDown2.Value + ";";

                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(query, conn);


                    //Set the SqlDataAdapter object
                    SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);

                    //define dataset
                    DataSet ds = new DataSet();

                    //fill dataset with query results
                    dAdapter.Fill(ds);

                    //set DataGridView control to read-only
                    grdData.ReadOnly = true;

                    //set the DataGridView control's data source/data table
                    grdData.DataSource = ds.Tables[0];

                    string query2 = @"SELECT Titre FROM dbo.Ouvrages WHERE Prix BETWEEN " + numericUpDown1.Value + " AND " + numericUpDown2.Value + ";";
                    SqlCommand cmd2 = new SqlCommand(query2, conn);
                    SqlDataAdapter dAdapter2 = new SqlDataAdapter(cmd2);
                    DataSet ds2 = new DataSet();
                    dAdapter.Fill(ds2);

                    //set the DataGridView control's data source/data table
                    listBox1.DataSource = ds2.Tables[0];
                    listBox1.DisplayMember = "Titre";
                }
            }
            catch (Exception ex)
            {
                //display error message
                MessageBox.Show("Exception: " + ex.Message);
            }
        }

        private void modeOffline_Click(object sender, EventArgs e)
        {
            //set the connection string
            string connString = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=Biblio;Integrated Security=True;Pooling=False";

            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    //retrieve the SQL Server instance version
                    string query = @"SELECT ID, Titre, ISBN, Prix FROM dbo.Ouvrages WHERE Prix BETWEEN " + numericUpDown1.Value + " AND " + numericUpDown2.Value + ";";

                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(query, conn);


                    //Set the SqlDataAdapter object
                    SqlDataAdapter dAdapter = new SqlDataAdapter(cmd);

                    //define dataset
                    DataSet ds = new DataSet();

                    //fill dataset with query results
                    dAdapter.Fill(ds);
                    Stream myStream;
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();

                    saveFileDialog1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
                    saveFileDialog1.FilterIndex = 2;
                    saveFileDialog1.RestoreDirectory = true;

                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        if ((myStream = saveFileDialog1.OpenFile()) != null)
                        {
                            StringWriter sw = new StringWriter();
                            ds.WriteXml(myStream);
                            // Code to write the stream goes here.
                            myStream.Close();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                //display error message
                MessageBox.Show("Exception: " + ex.Message);
            }
            
        }
    }
}
